/*
  -> During programming, the BCU may have no connection to the ARDUINO UNO.
  -> After programming for communication with the BCU. Connect the jumpers ICSP1 PIN 5 and ICSP1 PIN 6 together, tested only with ARDUINO UNO revision 3.
  -> For programming the jumper ICSP1 PIN 5 and ICSP1 PIN 6 must be not connected together and the voltage must be taken away for a short time. Then, you can transfer the new "sketch".
*/
#include <KnxTpUart.h> // https://bitbucket.org/MagGyver/arduino-am-knx

// Knx physical address of ARDUINO UNO
#define KNX_PA_AREA 15 // My physical address area
#define KNX_PA_LINE 15 // My physical address line
#define KNX_PA_MEMBER 20 // My physical address member

// Initialize the KNX TP-UART library on the serial port of ARDUINO UNO
KnxTpUart knx(&Serial, PA_INTEGER(KNX_PA_AREA, KNX_PA_LINE, KNX_PA_MEMBER));

int outLed = 13; // Output 13 (outLed on ARDUINO UNO)

byte targetGa[2]; // Variable for received group address

void setup() {

  pinMode(outLed, OUTPUT); // Set pin as output

  // Initialize the serial port with 19200 baudrate, 8 bit and even parity
  Serial.begin(19200, SERIAL_8E1);
  while (!Serial) {
  }
  knx.uartReset();
  delay(100);

  // TP-UART V1.05 ON ARDUINO UNO

  knx.addListenGroupAddress(GA_INTEGER(2, 6, 0)); // Adds group address which we are interested in
  knx.addListenGroupAddress(GA_INTEGER(5, 6, 0)); // Adds group address which we are interested in

  knx.groupRead(GA_INTEGER(2, 6, 0)); // Query the value or state of the group address -> evaluation in function "serialEvent()"
  knx.groupRead(GA_INTEGER(5, 6, 0)); // Query the value or state of the group address -> evaluation in function "serialEvent()"
}

void loop() {
}

void serialEvent() {
  KnxTpUartSerialEventType eType = knx.serialEvent();
  if (eType == KNX_TELEGRAM) {
    KnxTelegram* telegram = knx.getReceivedTelegram();
    if (!((telegram->getSourceArea() == KNX_PA_AREA) && (telegram->getSourceLine() == KNX_PA_LINE) && (telegram->getSourceMember() == KNX_PA_MEMBER))) { // Thus we do not respond to our data sent even
      if (telegram->isTargetGroup()) { // That have data received a group address?
        if (telegram->getCommand() == KNX_COMMAND_ANSWER) { // Is command answer command?
          telegram->getTarget(targetGa); // Save data receiving
          if (compareTarget(targetGa , GA_INTEGER(2, 6, 0))) { // Receiving data -> evaluate
            digitalWrite(outLed, telegram->getBool()); // Output 13 (outLed) on the received value set, 1 = on, 0 = off
          }
          else if (compareTarget(targetGa , GA_INTEGER(5, 6, 0))) {
            // From here your own processing of the above group address 5/6/0
          }
        }
      }
    }
  }
}

// Function for the reconciliation between received group address and our required group address
static bool compareTarget(byte byteArray1[2], byte byteArray2[2]) {
  for (int i = 0; i < 2; i++) {
    if (byteArray1[i] != byteArray2[i]) {
      return false;
    }
  }
  return true;
}