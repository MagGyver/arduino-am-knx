#include <KnxTpUart.h> // https://bitbucket.org/MagGyver/arduino-am-knx

// Knx physical address of ARDUINO MEGA
#define KNX_PA_AREA 15 // My physical address area
#define KNX_PA_LINE 15 // My physical address line
#define KNX_PA_MEMBER 20 // My physical address member

// Initialize the KNX TP-UART library on the serial1 port of ARDUINO MEGA
KnxTpUart knx(&Serial1, PA_INTEGER(KNX_PA_AREA, KNX_PA_LINE, KNX_PA_MEMBER));

int inPin = 32; // Input 32 (inPin on ARDUINO MEGA)

int outLed = 13; // Output 13 (outLed on ARDUINO MEGA)

byte targetGa[2]; // Variable for received group address

// Remember if we have sent on current keypress
boolean haveSent = false;

// Remember if we sent ON last or OFF
boolean onSent = false;


void setup() {
  pinMode(inPin, INPUT);
  digitalWrite(inPin, HIGH); // Turn on pullup

  pinMode(outLed, OUTPUT); // Set pin as output
  digitalWrite(outLed, LOW); // Set pin to low

  Serial.begin(9600);
  Serial.print("TP-UART VERSION");
  Serial.print(KNX_LIB_VERSION);
  Serial.println(" ON ARDUINO MEGA");

  // Initialize the serial1 port with 19200 baudrate, 8 bit and even parity
  Serial1.begin(19200, SERIAL_8E1);
  while (!Serial1) {
  }
  knx.uartReset();

  Serial.print("UCSR1A: ");
  Serial.println(UCSR1A, BIN);

  Serial.print("UCSR1B: ");
  Serial.println(UCSR1B, BIN);

  Serial.print("UCSR1C: ");
  Serial.println(UCSR1C, BIN);
}

void loop() {
  if (digitalRead(inPin) == LOW) {
    // Button is pressed
    digitalWrite(outLed, HIGH); // Set pin to low

    if (!haveSent) {
      // Send the opposite of what we have sent last
      bool success = knx.groupWriteBool(GA_INTEGER(0, 0, 3), !onSent);

      Serial.print("Successfully sent: ");
      Serial.println(!onSent);

      onSent = !onSent;
      haveSent = true;
    }
  } else {
    digitalWrite(outLed, LOW); // Set pin to low
    haveSent = false;
  }
}

void serialEvent1() {
  KnxTpUartSerialEventType eType = knx.serialEvent();
  if (eType == KNX_TELEGRAM) {
    KnxTelegram* telegram = knx.getReceivedTelegram();
    if (!((telegram->getSourceArea() == KNX_PA_AREA) && (telegram->getSourceLine() == KNX_PA_LINE) && (telegram->getSourceMember() == KNX_PA_MEMBER))) { // Thus we do not respond to our data sent even
      if (telegram->isTargetGroup()) { // That have data received a group address?
        // From here, see
        // in sketch "ReceiveKnxTelegram.ino"
        // or
        // in sketch "ReplyToKnxRead.ino"
        // for example.
      }
    }
  }
}

// Function for the reconciliation between received group address and our required group address
static bool compareTarget(byte byteArray1[2], byte byteArray2[2]) {
  for (int i = 0; i < 2; i++) {
    if (byteArray1[i] != byteArray2[i]) {
      return false;
    }
  }
  return true;
}