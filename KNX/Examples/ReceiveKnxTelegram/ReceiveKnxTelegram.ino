#include <KnxTpUart.h> // https://bitbucket.org/MagGyver/arduino-am-knx

// Knx physical address of ARDUINO MEGA
#define KNX_PA_AREA 15 // My physical address area
#define KNX_PA_LINE 15 // My physical address line
#define KNX_PA_MEMBER 20 // My physical address member

// Initialize the KNX TP-UART library on the serial1 port of ARDUINO MEGA
KnxTpUart knx(&Serial1, PA_INTEGER(KNX_PA_AREA, KNX_PA_LINE, KNX_PA_MEMBER));

int outLed = 13; // Output 13 (outLed on ARDUINO MEGA)

byte targetGa[2]; // Variable for received group address

void setup() {
  pinMode(outLed, OUTPUT); // Set pin as output

  Serial.begin(9600);
  Serial.print("TP-UART VERSION");
  Serial.print(KNX_LIB_VERSION);
  Serial.println(" ON ARDUINO MEGA");

  // Initialize the serial1 port with 19200 baudrate, 8 bit and even parity
  Serial1.begin(19200, SERIAL_8E1);
  while (!Serial1) {
  }
  knx.uartReset();

  knx.addListenGroupAddress(GA_INTEGER(15, 0, 0)); // Adds group address which we are interested in
  knx.addListenGroupAddress(GA_INTEGER(15, 0, 1)); // Adds group address which we are interested in
  knx.addListenGroupAddress(GA_INTEGER(15, 0, 2)); // Adds group address which we are interested in
  knx.addListenGroupAddress(GA_INTEGER(15, 0, 3)); // Adds group address which we are interested in
  knx.addListenGroupAddress(GA_INTEGER(15, 0, 4)); // Adds group address which we are interested in
  knx.addListenGroupAddress(GA_INTEGER(15, 0, 5)); // Adds group address which we are interested in
  knx.addListenGroupAddress(GA_INTEGER(15, 0, 6)); // Adds group address which we are interested in
  knx.addListenGroupAddress(GA_INTEGER(15, 0, 7)); // Adds group address which we are interested in
}

void loop() {
}

void serialEvent1() {
  KnxTpUartSerialEventType eType = knx.serialEvent();
  if (eType == KNX_TELEGRAM) {
    KnxTelegram* telegram = knx.getReceivedTelegram();
    if (!((telegram->getSourceArea() == KNX_PA_AREA) && (telegram->getSourceLine() == KNX_PA_LINE) && (telegram->getSourceMember() == KNX_PA_MEMBER))) { // Thus we do not respond to our data sent even
      if (telegram->isTargetGroup()) { // That have data received a group address?
        if (telegram->getCommand() == KNX_COMMAND_WRITE) { // Is command write command?
          telegram->getTarget(targetGa); // Save data receiving
          if (compareTarget(targetGa , GA_INTEGER(15, 0, 0))) { // Receiving data -> evaluate
            digitalWrite(outLed, telegram->getBool()); // Output 13 (outLed) on the received value set, 1 = on, 0 = off
            Serial.print("Received value for group address 15.0.0 :  ");
            Serial.println(telegram->getBool());
          }
          else if (compareTarget(targetGa , GA_INTEGER(15, 0, 1))) { // Receiving data -> evaluate
            Serial.print("Received value for group address 15.0.1 :  ");
            Serial.println(telegram->get1ByteIntValue());
          }
          else if (compareTarget(targetGa , GA_INTEGER(15, 0, 2))) { // Receiving data -> evaluate
            Serial.print("Received value for group address 15.0.2 :  ");
            Serial.println(telegram->get2ByteIntValue());
          }
          else if (compareTarget(targetGa , GA_INTEGER(15, 0, 3))) { // Receiving data -> evaluate
            Serial.print("Received value for group address 15.0.3 :  ");
            Serial.println(telegram->get2ByteFloatValue());
          }
          else if (compareTarget(targetGa , GA_INTEGER(15, 0, 4))) { // Receiving data -> evaluate
            Serial.print("Received value for group address 15.0.4 :  ");
            Serial.print(telegram->get3ByteHourValue());
            Serial.print(":");
            Serial.print(telegram->get3ByteMinuteValue());
            Serial.print(":");
            Serial.print(telegram->get3ByteSecondValue());
            Serial.println("  Time");
          }
          else if (compareTarget(targetGa , GA_INTEGER(15, 0, 5))) { // Receiving data -> evaluate
            Serial.print("Received value for group address 15.0.5 :  ");
            Serial.print(telegram->get3ByteDayValue());
            Serial.print(".");
            Serial.print(telegram->get3ByteMonthValue());
            Serial.print(".20");
            Serial.print(telegram->get3ByteYearValue());
            Serial.println("  Date");
          }
          else if (compareTarget(targetGa , GA_INTEGER(15, 0, 6))) { // Receiving data -> evaluate
            Serial.print("Received value for group address 15.0.6 :  ");
            Serial.println(telegram->get4ByteFloatValue());
          }
          else if (compareTarget(targetGa , GA_INTEGER(15, 0, 7))) { // Receiving data -> evaluate
            Serial.print("Received value for group address 15.0.7 :  ");
            Serial.println(telegram->get14ByteText());
          }
        }
      }
    }
  }
}

// Function for the reconciliation between received group address and our required group address
static bool compareTarget(byte byteArray1[2], byte byteArray2[2]) {
  for (int i = 0; i < 2; i++) {
    if (byteArray1[i] != byteArray2[i]) {
      return false;
    }
  }
  return true;
}