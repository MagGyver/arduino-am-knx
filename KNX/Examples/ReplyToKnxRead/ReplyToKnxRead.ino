#include <KnxTpUart.h> // https://bitbucket.org/MagGyver/arduino-am-knx

// Knx physical address of ARDUINO MEGA
#define KNX_PA_AREA 15 // My physical address area
#define KNX_PA_LINE 15 // My physical address line
#define KNX_PA_MEMBER 20 // My physical address member

// Initialize the KNX TP-UART library on the serial1 port of ARDUINO MEGA
KnxTpUart knx(&Serial1, PA_INTEGER(KNX_PA_AREA, KNX_PA_LINE, KNX_PA_MEMBER));

byte targetGa[2]; // Variable for received group address

void setup() {
  Serial.begin(9600);
  Serial.print("TP-UART VERSION");
  Serial.print(KNX_LIB_VERSION);
  Serial.println(" ON ARDUINO MEGA");

  // Initialize the serial1 port with 19200 baudrate, 8 bit and even parity
  Serial1.begin(19200, SERIAL_8E1);
  while (!Serial1) {
  }
  knx.uartReset();

  knx.addListenGroupAddress(GA_INTEGER(3, 0, 0)); // Adds group address which we are interested in
  knx.addListenGroupAddress(GA_INTEGER(3, 0, 1)); // Adds group address which we are interested in
  knx.addListenGroupAddress(GA_INTEGER(3, 0, 2)); // Adds group address which we are interested in
  knx.addListenGroupAddress(GA_INTEGER(3, 0, 3)); // Adds group address which we are interested in
  knx.addListenGroupAddress(GA_INTEGER(3, 0, 4)); // Adds group address which we are interested in
  knx.addListenGroupAddress(GA_INTEGER(3, 0, 5)); // Adds group address which we are interested in
  knx.addListenGroupAddress(GA_INTEGER(3, 0, 6)); // Adds group address which we are interested in
  knx.addListenGroupAddress(GA_INTEGER(3, 0, 7)); // Adds group address which we are interested in
}

void loop() {
}

void serialEvent1() {
  KnxTpUartSerialEventType eType = knx.serialEvent();
  if (eType == KNX_TELEGRAM) {
    KnxTelegram* telegram = knx.getReceivedTelegram();
    if (!((telegram->getSourceArea() == KNX_PA_AREA) && (telegram->getSourceLine() == KNX_PA_LINE) && (telegram->getSourceMember() == KNX_PA_MEMBER))) { // Thus we do not respond to our data sent even
      if (telegram->isTargetGroup()) { // That have data received a group address?
        if (telegram->getCommand() == KNX_COMMAND_READ) { // Is command read command?
          telegram->getTarget(targetGa); // Save data receiving
          if (compareTarget(targetGa , GA_INTEGER(3, 0, 0))) { // Receiving data -> evaluate
            knx.groupAnswerBool(GA_INTEGER(3, 0, 0), true);
            // Sent value to knx : 1
          }
          else if (compareTarget(targetGa , GA_INTEGER(3, 0, 1))) { // Receiving data -> evaluate
            knx.groupAnswer1ByteInt(GA_INTEGER(3, 0, 1), 126);
            // Sent value to knx : 49%
          }
          else if (compareTarget(targetGa , GA_INTEGER(3, 0, 2))) { // Receiving data -> evaluate
            knx.groupAnswer2ByteInt(GA_INTEGER(3, 0, 2), 1000.32);
            // Sent value to knx : 1000,32
          }
          else if (compareTarget(targetGa , GA_INTEGER(3, 0, 3))) { // Receiving data -> evaluate
            knx.groupAnswer2ByteFloat(GA_INTEGER(3, 0, 3), 25.28);
            // Sent value to knx : 25,28
          }
          else if (compareTarget(targetGa , GA_INTEGER(3, 0, 4))) { // Receiving data -> evaluate
            knx.groupAnswer3ByteTime(GA_INTEGER(3, 0, 4), 7, 12, 0, 0);
            // Sent value to knx : Sunday 12:00:00
          }
          else if (compareTarget(targetGa , GA_INTEGER(3, 0, 5))) { // Receiving data -> evaluate
            knx.groupAnswer3ByteDate(GA_INTEGER(3, 0, 5), 23, 1, 3);
            // Sent value to knx : 23.01.2003
          }
          else if (compareTarget(targetGa , GA_INTEGER(3, 0, 6))) { // Receiving data -> evaluate
            knx.groupAnswer4ByteFloat(GA_INTEGER(3, 0, 6), -100);
            // Sent value to knx : -100
          }
          else if (compareTarget(targetGa , GA_INTEGER(3, 0, 7))) { // Receiving data -> evaluate
            knx.groupAnswer14ByteText(GA_INTEGER(3, 0, 7), "Hallo");
            // Sent value to knx : Hallo
          }
        }
      }
    }
  }
}

// Function for the reconciliation between received group address and our required group address
static bool compareTarget(byte byteArray1[2], byte byteArray2[2]) {
  for (int i = 0; i < 2; i++) {
    if (byteArray1[i] != byteArray2[i]) {
      return false;
    }
  }
  return true;
}