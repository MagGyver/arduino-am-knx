/*
  File: KnxTelegram.cpp

  Author: Daniel Kleine-Albers
  Sourcecode: https://bitbucket.org/dka/arduino-tpuart

  Modified: tuxedo
  Modified: Mag Gyver

  Last modified: 13.08.2017

  V 1.00 - Optimizing memory consumption
  V 1.01 - DPT 3 added
  V 1.02 - Fixed error on initialization of serial port
  V 1.03 - Added link to library
  V 1.04 - Structure of versions changed
  V 1.05 - Additions with define added
  
  https://bitbucket.org/MagGyver/arduino-am-knx
*/

#include "KnxTelegram.h"

//  Public functions

KnxTelegram::KnxTelegram() {
  clear();
}

//-----

void KnxTelegram::clear() {
  for (int i = 0; i < MAX_KNX_TELEGRAM_SIZE; i++) {
    buffer[i] = 0;
  }
  buffer[0] = B10111100;
  buffer[5] = B11100001;
}

void KnxTelegram::setBufferByte(int index, int content) {
  buffer[index] = content;
}

int KnxTelegram::getBufferByte(int index) {
  return buffer[index];
}

void KnxTelegram::setPayloadLength(int length) {
  buffer[5] = buffer[5] & B11110000;
  buffer[5] = buffer[5] | (length - 1);
}

int KnxTelegram::getPayloadLength() {
  int length = (buffer[5] & B00001111) + 1;
  return length;
}

void KnxTelegram::setRepeated(bool repeat) {
  if (repeat) {
    buffer[0] = buffer[0] & B11011111;
  } else {
    buffer[0] = buffer[0] | B00100000;
  }
}

bool KnxTelegram::isRepeated() {
  if (buffer[0] & B00100000) {
    return false;
  } else {
    return true;
  }
}

void KnxTelegram::setPriority(KnxPriorityType prio) {
  buffer[0] = buffer[0] & B11110011;
  buffer[0] = buffer[0] | (prio << 2);
}

KnxPriorityType KnxTelegram::getPriority() {
  return (KnxPriorityType) ((buffer[0] & B00001100) >> 2);
}

//-----

void KnxTelegram::setSourceAddress(byte sourceAddress[2]) {
  buffer[1] = sourceAddress[0];
  buffer[2] = sourceAddress[1];
}

int KnxTelegram::getSourceArea() {
  return (buffer[1] >> 4);
}

int KnxTelegram::getSourceLine() {
  return (buffer[1] & B00001111);
}

int KnxTelegram::getSourceMember() {
  return buffer[2];
}

//-----

void KnxTelegram::setTargetGroupAddress(byte targetGroupAddress[2]) {
  buffer[3] = targetGroupAddress[0];
  buffer[4] = targetGroupAddress[1];
  buffer[5] = buffer[5] | B10000000;
}

int KnxTelegram::getTargetMainGroup() {
  return ((buffer[3] & B01111000) >> 3);
}

int KnxTelegram::getTargetMiddleGroup() {
  return (buffer[3] & B00000111);
}

int KnxTelegram::getTargetSubGroup() {
  return buffer[4];
}

//-----

void KnxTelegram::setTargetIndividualAddress(byte targetIndividualAddress[2]) {
  buffer[3] = targetIndividualAddress[0];
  buffer[4] = targetIndividualAddress[1];
  buffer[5] = buffer[5] & B01111111;
}

int KnxTelegram::getTargetArea() {
  return ((buffer[3] & B11110000) >> 4);
}

int KnxTelegram::getTargetLine() {
  return (buffer[3] & B00001111);
}

int KnxTelegram::getTargetMember() {
  return buffer[4];
}

//-----

void KnxTelegram::getTarget(byte target[2]) {
  target[0] = buffer[3];
  target[1] = buffer[4];
}

bool KnxTelegram::isTargetGroup() {
  return buffer[5] & B10000000;
}

bool KnxTelegram::isBroadcast() {
  return isTargetGroup() && buffer[3] == 0 && buffer[4] == 0;
}

//-----

void KnxTelegram::setRoutingCounter(int counter) {
  buffer[5] = buffer[5] & B10000000;
  buffer[5] = buffer[5] | (counter << 4);
}

int KnxTelegram::getRoutingCounter() {
  return ((buffer[5] & B01110000) >> 4);
}

//-----

void KnxTelegram::setCommand(KnxCommandType command) {
  buffer[6] = buffer[6] & B11111100;
  buffer[7] = buffer[7] & B00111111;

  buffer[6] = buffer[6] | (command >> 2);
  buffer[7] = buffer[7] | (command << 6);
}

KnxCommandType KnxTelegram::getCommand() {
  return (KnxCommandType) (((buffer[6] & B00000011) << 2) | ((buffer[7] & B11000000) >> 6));
}

//-----

void KnxTelegram::setExtendedCommand(KnxExtendedCommandType extCommand) {
  buffer[7] = buffer[7] & B11000000;
  buffer[7] = buffer[7] | (extCommand >> 6);
}

KnxExtendedCommandType KnxTelegram::getExtendedCommand() {
  return (KnxExtendedCommandType) (buffer[7] & B00111111);
}

//-----

void KnxTelegram::createChecksum() {
  int checksumPos = getPayloadLength() + KNX_TELEGRAM_HEADER_SIZE;
  buffer[checksumPos] = calculateChecksum();
}

bool KnxTelegram::verifyChecksum() {
  int calculatedChecksum = calculateChecksum();
  return (getChecksum() == calculatedChecksum);
}

int KnxTelegram::getChecksum() {
  int checksumPos = getPayloadLength() + KNX_TELEGRAM_HEADER_SIZE;
  return buffer[checksumPos];
}

void KnxTelegram::print(TPUART_SERIAL_CLASS* serial) {
#if defined(TPUART_DEBUG)
  serial->print("Repeated: ");
  serial->println(isRepeated());

  serial->print("Priority: ");
  serial->println(getPriority());

  serial->print("Source: ");
  serial->print(getSourceArea());
  serial->print(".");
  serial->print(getSourceLine());
  serial->print(".");
  serial->println(getSourceMember());

  if (isTargetGroup()) {
    serial->print("Target Group: ");
    serial->print(getTargetMainGroup());
    serial->print("/");
    serial->print(getTargetMiddleGroup());
    serial->print("/");
    serial->println(getTargetSubGroup());
  } else {
    serial->print("Target Physical: ");
    serial->print(getTargetArea());
    serial->print(".");
    serial->print(getTargetLine());
    serial->print(".");
    serial->println(getTargetMember());
  }

  serial->print("Routing Counter: ");
  serial->println(getRoutingCounter());

  serial->print("Payload Length: ");
  serial->println(getPayloadLength());

  serial->print("Command: ");
  serial->println(getCommand());

  serial->print("First Data Byte: ");
  serial->println(getFirstDataByte());

  for (int i = 2; i < getPayloadLength(); i++) {
    serial->print("Data Byte ");
    serial->print(i);
    serial->print(": ");
    serial->println(buffer[6 + i], BIN);
  }


  if (verifyChecksum()) {
    serial->println("Checksum matches");
  } else {
    serial->println("Checksum mismatch");
    serial->println(getChecksum(), BIN);
    serial->println(calculateChecksum(), BIN);
  }
#endif
}

int KnxTelegram::getTotalLength() {
  return KNX_TELEGRAM_HEADER_SIZE + getPayloadLength() + 1;
}

void KnxTelegram::setCommunicationType(KnxCommunicationType type) {
  buffer[6] = buffer[6] & B00111111;
  buffer[6] = buffer[6] | (type << 6);
}

KnxCommunicationType KnxTelegram::getCommunicationType() {
  return (KnxCommunicationType) ((buffer[6] & B11000000) >> 6);
}

//-----

void KnxTelegram::setSequenceNumber(int number) {
  buffer[6] = buffer[6] & B11000011;
  buffer[6] = buffer[6] | (number << 2);
}

int KnxTelegram::getSequenceNumber() {
  return (buffer[6] & B00111100) >> 2;
}

//-----

void KnxTelegram::setControlData(KnxControlDataType cd) {
  buffer[6] = buffer[6] & B11111100;
  buffer[6] = buffer[6] | cd;
}

KnxControlDataType KnxTelegram::getControlData() {
  return (KnxControlDataType) (buffer[6] & B00000011);
}

//  Setter+Getter for DPTs

void KnxTelegram::setFirstDataByte(int data) {
  buffer[7] = buffer[7] & B11000000;
  buffer[7] = buffer[7] | data;
}

int KnxTelegram::getFirstDataByte() {
  return (buffer[7] & B00111111);
}

bool KnxTelegram::getBool() {
  if (getPayloadLength() != 2) {
    // Wrong payload length
    return 0;
  }
  return (getFirstDataByte() & B00000001);
}

//-----

int KnxTelegram::get4BitIntValue() {
  if (getPayloadLength() != 2) {
    // Wrong payload length
    return 0;
  }
  return (getFirstDataByte() & B00001111);
}

bool KnxTelegram::get4BitDirectionValue() {
  if (getPayloadLength() != 2) {
    // Wrong payload length
    return 0;
  }
  return ((getFirstDataByte() & B00001000)) >> 3;
}

byte KnxTelegram::get4BitStepsValue() {
  if (getPayloadLength() != 2) {
    // Wrong payload length
    return 0;
  }
  return (getFirstDataByte() & B00000111);
}

//-----

void KnxTelegram::set1ByteIntValue(int value) {
  setPayloadLength(3);
  buffer[8] = value;
}

int KnxTelegram::get1ByteIntValue() {
  if (getPayloadLength() != 3) {
    // Wrong payload length
    return 0;
  }
  return (buffer[8]);
}

//-----

void KnxTelegram::set2ByteIntValue(int value) {
  setPayloadLength(4);

  buffer[8] = byte(value >> 8);
  buffer[9] = byte(value & 0x00FF);
}

int KnxTelegram::get2ByteIntValue() {
  if (getPayloadLength() != 4) {
    // Wrong payload length
    return 0;
  }
  int value = int(buffer[8] << 8) + int(buffer[9]);

  return (value);
}

//-----

void KnxTelegram::set2ByteFloatValue(float value) {
  setPayloadLength(4);

  float v = value * 100.0f;
  int exponent = 0;
  for (; v < -2048.0f; v /= 2) exponent++;
  for (; v > 2047.0f; v /= 2) exponent++;
  long m = round(v) & 0x7FF;
  short msb = (short) (exponent << 3 | m >> 8);
  if (value < 0.0f) msb |= 0x80;
  buffer[8] = msb;
  buffer[9] = (byte)m;
}

float KnxTelegram::get2ByteFloatValue() {
  if (getPayloadLength() != 4) {
    // Wrong payload length
    return 0;
  }
  int exponent = (buffer[8] & B01111000) >> 3;
  int mantissa = ((buffer[8] & B00000111) << 8) | (buffer[9]);
  if (buffer[8] & B10000000) {
    return ((-2048 + mantissa) * 0.01) * pow(2.0, exponent);
  }
  return (mantissa * 0.01) * pow(2.0, exponent);
}

//-----

void KnxTelegram::set3ByteTime(int weekday, int hours, int minutes, int seconds) {
  setPayloadLength(5);

  weekday = weekday << 5;
  buffer[8] = (weekday & B11100000) + (hours & B00011111);
  buffer[9] =  minutes & B00111111;
  buffer[10] = seconds & B00111111;
}

int KnxTelegram::get3ByteWeekdayValue() {
  if (getPayloadLength() != 5) {
    // Wrong payload length
    return 0;
  }
  return (buffer[8] & B11100000) >> 5;
}

int KnxTelegram::get3ByteHourValue() {
  if (getPayloadLength() != 5) {
    // Wrong payload length
    return 0;
  }
  return (buffer[8] & B00011111);
}

int KnxTelegram::get3ByteMinuteValue() {
  if (getPayloadLength() != 5) {
    // Wrong payload length
    return 0;
  }
  return (buffer[9] & B00111111);
}

int KnxTelegram::get3ByteSecondValue() {
  if (getPayloadLength() != 5) {
    // Wrong payload length
    return 0;
  }
  return (buffer[10] & B00111111);
}

//-----

void KnxTelegram::set3ByteDate(int day, int month, int year) {
  setPayloadLength(5);

  buffer[8] = day & B00011111;
  buffer[9] =  month & B00001111;
  buffer[10] = year;
}

int KnxTelegram::get3ByteDayValue() {
  if (getPayloadLength() != 5) {
    // Wrong payload length
    return 0;
  }
  return (buffer[8] & B00011111);
}

int KnxTelegram::get3ByteMonthValue() {
  if (getPayloadLength() != 5) {
    // Wrong payload length
    return 0;
  }
  return (buffer[9] & B00001111);
}

int KnxTelegram::get3ByteYearValue() {
  if (getPayloadLength() != 5) {
    // Wrong payload length
    return 0;
  }
  return (buffer[10]);
}

//-----

void KnxTelegram::set4ByteFloatValue(float value) {
  setPayloadLength(6);

  byte b[4];
  float *f = (float*)(void*) & (b[0]);
  *f = value;

  buffer[8 + 3] = b[0];
  buffer[8 + 2] = b[1];
  buffer[8 + 1] = b[2];
  buffer[8 + 0] = b[3];
}

float KnxTelegram::get4ByteFloatValue() {
  if (getPayloadLength() != 6) {
    return 0;
  }
  byte b[4];
  b[0] = buffer[8 + 3];
  b[1] = buffer[8 + 2];
  b[2] = buffer[8 + 1];
  b[3] = buffer[8 + 0];
  float *f = (float*)(void*) & (b[0]);
  float  r = *f;
  return r;
}

//-----

void KnxTelegram::set14ByteText(String value) {
  char _load[15];
  for (int i = 0; i < 14; ++i)
  {
    _load[i] = 0;
  }
  setPayloadLength(16);

  value.toCharArray(_load, 15);
  buffer[8 + 0] = _load [0];
  buffer[8 + 1] = _load [1];
  buffer[8 + 2] = _load [2];
  buffer[8 + 3] = _load [3];
  buffer[8 + 4] = _load [4];
  buffer[8 + 5] = _load [5];
  buffer[8 + 6] = _load [6];
  buffer[8 + 7] = _load [7];
  buffer[8 + 8] = _load [8];
  buffer[8 + 9] = _load [9];
  buffer[8 + 10] = _load [10];
  buffer[8 + 11] = _load [11];
  buffer[8 + 12] = _load [12];
  buffer[8 + 13] = _load [13];
}

String KnxTelegram::get14ByteText() {
  if (getPayloadLength() != 16) {
    return "";
  }
  char _load[15];
  _load[0] = buffer[8 + 0];
  _load[1] = buffer[8 + 1];
  _load[2] = buffer[8 + 2];
  _load[3] = buffer[8 + 3];
  _load[4] = buffer[8 + 4];
  _load[5] = buffer[8 + 5];
  _load[6] = buffer[8 + 6];
  _load[7] = buffer[8 + 7];
  _load[8] = buffer[8 + 8];
  _load[9] = buffer[8 + 9];
  _load[10] = buffer[8 + 10];
  _load[11] = buffer[8 + 11];
  _load[12] = buffer[8 + 12];
  _load[13] = buffer[8 + 13];
  return (_load);
}

// Private functions

int KnxTelegram::calculateChecksum() {
  int bcc = 0xFF;
  int size = getPayloadLength() + KNX_TELEGRAM_HEADER_SIZE;

  for (int i = 0; i < size; i++) {
    bcc ^= buffer[i];
  }
  return bcc;
}