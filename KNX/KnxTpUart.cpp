/*
  File: KnxTpUart.cpp

  Author: Daniel Kleine-Albers
  Sourcecode: https://bitbucket.org/dka/arduino-tpuart

  Modified: tuxedo
  Modified: Mag Gyver

  Last modified: 13.08.2017

  V 1.00 - Optimizing memory consumption
  V 1.01 - DPT 3 added
  V 1.02 - Fixed error on initialization of serial port
  V 1.03 - Added link to library
  V 1.04 - Structure of versions changed
  V 1.05 - Additions with define added
  
  https://bitbucket.org/MagGyver/arduino-am-knx
*/

#include "KnxTpUart.h"

//  Public functions

KnxTpUart::KnxTpUart(TPUART_SERIAL_CLASS* sport, byte address[2]) {
  _serialport = sport;
  _individual_address[0] = address[0];
  _individual_address[1] = address[1];
  _listen_group_address_count = 0;
  _tg = new KnxTelegram();
  _tg_ptp = new KnxTelegram();
  _listen_to_broadcasts = false;
}

void KnxTpUart::uartReset() {
  _serialport->write(TPUART_RESET_REQUEST);
}

void KnxTpUart::uartStateRequest() {
  _serialport->write(TPUART_STATE_REQUEST);
}

void KnxTpUart::addListenGroupAddress(byte address[]) {
  if (_listen_group_address_count >= MAX_LISTEN_GROUP_ADDRESSES) {
#if defined(TPUART_DEBUG)
    TPUART_DEBUG_PORT.println("Already listening to MAX_LISTEN_GROUP_ADDRESSES, cannot listen to another");
#endif
    return;
  }
  _listen_group_addresses[_listen_group_address_count][0] = address[0];
  _listen_group_addresses[_listen_group_address_count][1] = address[1];
  _listen_group_address_count++;
  /*
    #if defined(TPUART_DEBUG)
    for (int i = 0; i < _listen_group_address_count; i++) {

      TPUART_DEBUG_PORT.print("Listen for: [");
      TPUART_DEBUG_PORT.print(i);
      TPUART_DEBUG_PORT.print("] -> ");
      TPUART_DEBUG_PORT.print(_listen_group_addresses[i][0]);
      TPUART_DEBUG_PORT.print("/");
      TPUART_DEBUG_PORT.print(_listen_group_addresses[i][1]);
      TPUART_DEBUG_PORT.print("/");
      TPUART_DEBUG_PORT.print(_listen_group_addresses[i][2]);
      TPUART_DEBUG_PORT.println("");
    }
    #endif
  */
}

KnxTpUartSerialEventType KnxTpUart::serialEvent() {
  while (_serialport->available() > 0) {
#if defined(TPUART_DEBUG)
    checkErrors();
#endif
    int incomingByte = _serialport->peek();
#if defined(TPUART_DEBUG)
    printByte(incomingByte);
#endif
    if (isKNXControlByte(incomingByte)) {
      bool interested = readKNXTelegram();
      if (interested) {
#if defined(TPUART_DEBUG)
        TPUART_DEBUG_PORT.println("Event KNX_TELEGRAM");
#endif
        return KNX_TELEGRAM;
      } else {
#if defined(TPUART_DEBUG)
        TPUART_DEBUG_PORT.println("Event IRRELEVANT_KNX_TELEGRAM");
#endif
        return IRRELEVANT_KNX_TELEGRAM;
      }
    } else if (incomingByte == TPUART_RESET_INDICATION_BYTE) {
      serialRead();
#if defined(TPUART_DEBUG)
      TPUART_DEBUG_PORT.println("Event TPUART_RESET_INDICATION");
#endif
      return TPUART_RESET_INDICATION;
    } else {
      serialRead();
#if defined(TPUART_DEBUG)
      TPUART_DEBUG_PORT.println("Event UNKNOWN");
#endif
      return UNKNOWN;
    }
  }
#if defined(TPUART_DEBUG)
  TPUART_DEBUG_PORT.println("Event UNKNOWN");
#endif
  return UNKNOWN;
}

KnxTelegram* KnxTpUart::getReceivedTelegram() {
  return _tg;
}

//-----

bool KnxTpUart::groupWriteBool(byte groupAddress[2], bool value) {
  int valueAsInt = 0;
  if (value) {
    valueAsInt = B00000001;
  }
  createKNXMessageFrame(2, KNX_COMMAND_WRITE, groupAddress, valueAsInt);
  return sendMessage();
}

bool KnxTpUart::groupWrite4BitInt(byte groupAddress[2], int value) {
  int valueAsInt = 0;
  if (value) {
    valueAsInt = value & B00001111;
  }
  createKNXMessageFrame(2, KNX_COMMAND_WRITE, groupAddress, valueAsInt);
  return sendMessage();
}

bool KnxTpUart::groupWrite4BitDim(byte groupAddress[2], bool direction, byte steps) {
  int valueAsInt = 0;
  if (direction || steps) {
    valueAsInt = (direction << 3) + (steps & B00000111);
  }
  createKNXMessageFrame(2, KNX_COMMAND_WRITE, groupAddress, valueAsInt);
  return sendMessage();
}

bool KnxTpUart::groupWrite1ByteInt(byte groupAddress[2], int value) {
  createKNXMessageFrame(2, KNX_COMMAND_WRITE, groupAddress, 0);
  _tg->set1ByteIntValue(value);
  _tg->createChecksum();
  return sendMessage();
}

bool KnxTpUart::groupWrite2ByteInt(byte groupAddress[2], int value) {
  createKNXMessageFrame(2, KNX_COMMAND_WRITE, groupAddress, 0);
  _tg->set2ByteFloatValue(value);
  _tg->createChecksum();
  return sendMessage();
}

bool KnxTpUart::groupWrite2ByteFloat(byte groupAddress[2], float value) {
  createKNXMessageFrame(2, KNX_COMMAND_WRITE, groupAddress, 0);
  _tg->set2ByteFloatValue(value);
  _tg->createChecksum();
  return sendMessage();
}

bool KnxTpUart::groupWrite3ByteTime(byte groupAddress[2], int weekday, int hour, int minute, int second) {
  createKNXMessageFrame(2, KNX_COMMAND_WRITE, groupAddress, 0);
  _tg->set3ByteTime(weekday, hour, minute, second);
  _tg->createChecksum();
  return sendMessage();
}

bool KnxTpUart::groupWrite3ByteDate(byte groupAddress[2], int day, int month, int year) {
  createKNXMessageFrame(2, KNX_COMMAND_WRITE, groupAddress, 0);
  _tg->set3ByteDate(day, month, year);
  _tg->createChecksum();
  return sendMessage();
}

bool KnxTpUart::groupWrite4ByteFloat(byte groupAddress[2], float value) {
  createKNXMessageFrame(2, KNX_COMMAND_WRITE, groupAddress, 0);
  _tg->set4ByteFloatValue(value);
  _tg->createChecksum();
  return sendMessage();
}

bool KnxTpUart::groupWrite14ByteText(byte groupAddress[2], String value) {
  createKNXMessageFrame(2, KNX_COMMAND_WRITE, groupAddress, 0);
  _tg->set14ByteText(value);
  _tg->createChecksum();
  return sendMessage();
}

//-----

bool KnxTpUart::groupAnswerBool(byte groupAddress[2], bool value) {
  int valueAsInt = 0;
  if (value) {
    valueAsInt = B00000001;
  }
  createKNXMessageFrame(2, KNX_COMMAND_ANSWER, groupAddress, valueAsInt);
  return sendMessage();
}

/*
  bool KnxTpUart::groupAnswerBitInt(byte groupAddress[2], int value) {
  int valueAsInt = 0;
  if (value) {
    valueAsInt = value & B00001111;
  }
  createKNXMessageFrame(2, KNX_COMMAND_ANSWER, groupAddress, valueAsInt);
  return sendMessage();
  }

  bool KnxTpUart::groupAnswer4BitDim(byte groupAddress[2], bool direction, byte steps) {
  int valueAsInt = 0;
  if (direction || steps) {
    valueAsInt = (direction << 3) + (steps & B00000111);
  }
  createKNXMessageFrame(2, KNX_COMMAND_ANSWER, groupAddress, valueAsInt);
  return sendMessage();
  }
*/

bool KnxTpUart::groupAnswer1ByteInt(byte groupAddress[2], int value) {
  createKNXMessageFrame(2, KNX_COMMAND_ANSWER, groupAddress, 0);
  _tg->set1ByteIntValue(value);
  _tg->createChecksum();
  return sendMessage();
}

bool KnxTpUart::groupAnswer2ByteInt(byte groupAddress[2], int value) {
  createKNXMessageFrame(2, KNX_COMMAND_ANSWER, groupAddress, 0);
  _tg->set2ByteFloatValue(value);
  _tg->createChecksum();
  return sendMessage();
}

bool KnxTpUart::groupAnswer2ByteFloat(byte groupAddress[2], float value) {
  createKNXMessageFrame(2, KNX_COMMAND_ANSWER, groupAddress, 0);
  _tg->set2ByteFloatValue(value);
  _tg->createChecksum();
  return sendMessage();
}

bool KnxTpUart::groupAnswer3ByteTime(byte groupAddress[2], int weekday, int hour, int minute, int second) {
  createKNXMessageFrame(2, KNX_COMMAND_ANSWER, groupAddress, 0);
  _tg->set3ByteTime(weekday, hour, minute, second);
  _tg->createChecksum();
  return sendMessage();
}

bool KnxTpUart::groupAnswer3ByteDate(byte groupAddress[2], int day, int month, int year) {
  createKNXMessageFrame(2, KNX_COMMAND_ANSWER, groupAddress, 0);
  _tg->set3ByteDate(day, month, year);
  _tg->createChecksum();
  return sendMessage();
}

bool KnxTpUart::groupAnswer4ByteFloat(byte groupAddress[2], float value) {
  createKNXMessageFrame(2, KNX_COMMAND_ANSWER, groupAddress, 0);
  _tg->set4ByteFloatValue(value);
  _tg->createChecksum();
  return sendMessage();
}

bool KnxTpUart::groupAnswer14ByteText(byte groupAddress[2], String value) {
  createKNXMessageFrame(2, KNX_COMMAND_ANSWER, groupAddress, 0);
  _tg->set14ByteText(value);
  _tg->createChecksum();
  return sendMessage();
}

//-----

bool KnxTpUart::groupRead(byte groupAddress[2]) {
  createKNXMessageFrame(2, KNX_COMMAND_READ, groupAddress, 0);
  _tg->createChecksum();
  return sendMessage();
}

//-----

void KnxTpUart::setListenToBroadcasts(bool listen) {
  _listen_to_broadcasts = listen;
}

void KnxTpUart::setIndividualAddress(byte address[2]) {
  _individual_address[0] = address[0];
  _individual_address[1] = address[1];
}

bool KnxTpUart::individualAnswerAddress() {
  createKNXMessageFrame(2, KNX_COMMAND_INDIVIDUAL_ADDR_RESPONSE, PA_INTEGER(0, 0, 0), 0);
  _tg->createChecksum();
  return sendMessage();
}

bool KnxTpUart::individualAnswerMaskVersion(int area, int line, int member) {
  createKNXMessageFrameIndividual(4, KNX_COMMAND_MASK_VERSION_RESPONSE, PA_INTEGER(area, line, member), 0);
  _tg->setCommunicationType(KNX_COMM_NDP);
  _tg->setBufferByte(8, 0x07);
  _tg->setBufferByte(9, 0x01);
  _tg->createChecksum();
  return sendMessage();
}

bool KnxTpUart::individualAnswerAuth(int accessLevel, int sequenceNo, int area, int line, int member) {
  createKNXMessageFrameIndividual(3, KNX_COMMAND_ESCAPE, PA_INTEGER(area, line, member), KNX_EXT_COMMAND_AUTH_RESPONSE);
  _tg->setCommunicationType(KNX_COMM_NDP);
  _tg->setSequenceNumber(sequenceNo);
  _tg->setBufferByte(8, accessLevel);
  _tg->createChecksum();
  return sendMessage();
}

// Private functions

void KnxTpUart::sendAck() {
#if defined(TPUART_DEBUG)
  TPUART_DEBUG_PORT.print("Send ACK");
#endif
  _serialport->write(TPUART_ACK_SERVICE_ADDRESSED);
  delay (SERIAL_WRITE_DELAY_MS);
}

void KnxTpUart::sendNotAddressed() {
  _serialport->write(TPUART_ACK_SERVICE_NOT_ADDRESSED);
  delay (SERIAL_WRITE_DELAY_MS);
}

bool KnxTpUart::isListeningToGroupAddress(byte address[2]) {


  for (int i = 0; i < _listen_group_address_count; i++) {

    if ( (_listen_group_addresses[i][0] == address[0])
         && (_listen_group_addresses[i][1] == address[1])) {
      return true;
    }
  }

  return false;
}

bool KnxTpUart::isKNXControlByte(int b) {
  return ( (b | B00101100) == B10111100 );
}

void KnxTpUart::checkErrors() {
#if defined(_SAM3XA_)
  if (USART1->US_CSR & US_CSR_OVRE) {
    TPUART_DEBUG_PORT.println("Overrun");
  }

  if (USART1->US_CSR & US_CSR_FRAME) {
    TPUART_DEBUG_PORT.println("Frame Error");
  }

  if (USART1->US_CSR & US_CSR_PARE) {
    TPUART_DEBUG_PORT.println("Parity Error");
  }
#elif defined(__AVR_ATmega168__) || defined(__AVR_ATmega328P__)
  if (UCSR0A & B00010000) {
    TPUART_DEBUG_PORT.println("Frame Error");
  }

  if (UCSR0A & B00000100) {
    TPUART_DEBUG_PORT.println("Parity Error");
  }
#else
  if (UCSR1A & B00010000) {
    TPUART_DEBUG_PORT.println("Frame Error");
  }

  if (UCSR1A & B00000100) {
    TPUART_DEBUG_PORT.println("Parity Error");
  }
#endif
}

void KnxTpUart::printByte(int incomingByte) {
  TPUART_DEBUG_PORT.print("Incoming Byte: ");
  TPUART_DEBUG_PORT.print(incomingByte, DEC);
  TPUART_DEBUG_PORT.print(" - ");
  TPUART_DEBUG_PORT.print(incomingByte, HEX);
  TPUART_DEBUG_PORT.print(" - ");
  TPUART_DEBUG_PORT.print(incomingByte, BIN);
  TPUART_DEBUG_PORT.println();
}

bool KnxTpUart::readKNXTelegram() {
  for (int i = 0; i < 6; i++) {
    _tg->setBufferByte(i, serialRead());
  }
#if defined(TPUART_DEBUG)
  TPUART_DEBUG_PORT.print("Payload Length: ");
  TPUART_DEBUG_PORT.println(_tg->getPayloadLength());
#endif
  int bufpos = 6;
  for (int i = 0; i < _tg->getPayloadLength(); i++) {
    _tg->setBufferByte(bufpos, serialRead());
    bufpos++;
  }
  _tg->setBufferByte(bufpos, serialRead());
#if defined(TPUART_DEBUG)
  _tg->print(&TPUART_DEBUG_PORT);
#endif
  byte target[2];
  _tg->getTarget(target);
  bool interestedGA = _tg->isTargetGroup() && isListeningToGroupAddress(target);
  bool interestedPA = ((!_tg->isTargetGroup()) && target[0] == _individual_address[0] && target[1] == _individual_address[1]);
  bool interestedBC = (_listen_to_broadcasts && _tg->isBroadcast());
#if defined(TPUART_DEBUG)
  TPUART_DEBUG_PORT.print("Interested GA: ");
  TPUART_DEBUG_PORT.println(interestedGA);
  TPUART_DEBUG_PORT.print("Interested PA: ");
  TPUART_DEBUG_PORT.println(interestedPA);
  TPUART_DEBUG_PORT.print("Interested BC: ");
  TPUART_DEBUG_PORT.println(interestedBC);

  TPUART_DEBUG_PORT.print("target: [0]=");
  TPUART_DEBUG_PORT.print(target[0]);
  TPUART_DEBUG_PORT.print(" [1]=");
  TPUART_DEBUG_PORT.print(target[1]);
  TPUART_DEBUG_PORT.println();
#endif
  bool interested = interestedGA || interestedPA || interestedBC;
  if (interested) {
    sendAck();
  } else {
    sendNotAddressed();
  }
  if (_tg->getCommunicationType() == KNX_COMM_UCD) {
#if defined(TPUART_DEBUG)
    TPUART_DEBUG_PORT.println("UCD Telegram received");
#endif
  } else if (_tg->getCommunicationType() == KNX_COMM_NCD) {
#if defined(TPUART_DEBUG)
    TPUART_DEBUG_PORT.print("NCD Telegram ");
    TPUART_DEBUG_PORT.print(_tg->getSequenceNumber());
    TPUART_DEBUG_PORT.println(" received");
#endif
    if (interested) {
      sendNCDPosConfirm(_tg->getSequenceNumber(), PA_INTEGER(_tg->getSourceArea(), _tg->getSourceLine(), _tg->getSourceMember()));
    }
  }
  return interested;
}

void KnxTpUart::createKNXMessageFrame(int payloadlength, KnxCommandType command, byte groupAddress[2], int firstDataByte) {
  _tg->clear();
  _tg->setSourceAddress(_individual_address);
  _tg->setTargetGroupAddress(groupAddress);
  _tg->setFirstDataByte(firstDataByte);
  _tg->setCommand(command);
  _tg->setPayloadLength(payloadlength);
  _tg->createChecksum();
}

void KnxTpUart::createKNXMessageFrameIndividual(int payloadlength, KnxCommandType command, byte targetIndividualAddress[2], int firstDataByte) {
  _tg->clear();
  _tg->setSourceAddress(_individual_address);
  _tg->setTargetIndividualAddress(targetIndividualAddress);
  _tg->setFirstDataByte(firstDataByte);
  _tg->setCommand(command);
  _tg->setPayloadLength(payloadlength);
  _tg->createChecksum();
}

bool KnxTpUart::sendMessage() {
  int messageSize = _tg->getTotalLength();

  uint8_t sendbuf[2];
  for (int i = 0; i < messageSize; i++) {
    if (i == (messageSize - 1)) {
      sendbuf[0] = TPUART_DATA_END_REQUEST;
    }
    else {
      sendbuf[0] = TPUART_DATA_START_CONTINUE_REQUEST;
    }
    sendbuf[0] |= i;
    sendbuf[1] = _tg->getBufferByte(i);

    _serialport->write(sendbuf, 2);
  }
  byte confirmation;
  while (true) {
    confirmation = serialRead();
    if (confirmation == TPUART_DATA_CONFIRM_SUCCESS_BYTE) {
      delay (SERIAL_WRITE_DELAY_MS);
      return true;
    }
    else if (confirmation == TPUART_DATA_CONFIRM_FAILED_BYTE) {
      delay (SERIAL_WRITE_DELAY_MS);
      return false;
    }
    else if (_confirmation_timeout == 1) {
      delay (SERIAL_WRITE_DELAY_MS);
      _confirmation_timeout = 0;
      return false;
    }
  }
  return false;
}

bool KnxTpUart::sendNCDPosConfirm(int sequenceNo, byte targetIndividualAddress[2]) {
  _tg_ptp->clear();
  _tg_ptp->setSourceAddress(_individual_address);
  _tg_ptp->setTargetIndividualAddress(targetIndividualAddress);
  _tg_ptp->setSequenceNumber(sequenceNo);
  _tg_ptp->setCommunicationType(KNX_COMM_NCD);
  _tg_ptp->setControlData(KNX_CONTROLDATA_POS_CONFIRM);
  _tg_ptp->setPayloadLength(1);
  _tg_ptp->createChecksum();

  int messageSize = _tg_ptp->getTotalLength();

  uint8_t sendbuf[2];
  for (int i = 0; i < messageSize; i++) {
    if (i == (messageSize - 1)) {
      sendbuf[0] = TPUART_DATA_END_REQUEST;
    }
    else {
      sendbuf[0] = TPUART_DATA_START_CONTINUE_REQUEST;
    }
    sendbuf[0] |= i;
    sendbuf[1] = _tg_ptp->getBufferByte(i);
    _serialport->write(sendbuf, 2);
  }
  byte confirmation;
  while (true) {
    confirmation = serialRead();
    if (confirmation == TPUART_DATA_CONFIRM_SUCCESS_BYTE) {
      return true;
    }
    else if (confirmation == TPUART_DATA_CONFIRM_FAILED_BYTE) {
      return false;
    }
    else if (_confirmation_timeout == 1) {
      _confirmation_timeout = 0;
      return false;
    }
  }
  return false;
}

int KnxTpUart::serialRead() {
  unsigned long startTime = millis();
#if defined(TPUART_DEBUG)
  TPUART_DEBUG_PORT.print("Available: ");
  TPUART_DEBUG_PORT.println(_serialport->available());
#endif

  while (! (_serialport->available() > 0)) {
    if (abs(millis() - startTime) > SERIAL_READ_TIMEOUT_MS) {
#if defined(TPUART_DEBUG)
      TPUART_DEBUG_PORT.println("Timeout while receiving message");
#endif
      _confirmation_timeout = 1;
    }
    delay (1);
  }
  byte inByte = _serialport->read();
#if defined(TPUART_DEBUG)
  checkErrors();
  printByte(inByte);
#endif
  return inByte;
}