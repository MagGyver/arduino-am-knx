/*
  File: KnxTpUart.h

  Author: Daniel Kleine-Albers
  Sourcecode: https://bitbucket.org/dka/arduino-tpuart

  Modified: tuxedo
  Modified: Mag Gyver

  Last modified: 13.08.2017

  V 1.00 - Optimizing memory consumption
  V 1.01 - DPT 3 added
  V 1.02 - Fixed error on initialization of serial port
  V 1.03 - Added link to library
  V 1.04 - Structure of versions changed
  V 1.05 - Additions with define added
  
  https://bitbucket.org/MagGyver/arduino-am-knx
*/

#ifndef KnxTpUart_h
#define KnxTpUart_h

#define KNX_LIB_VERSION 1.05 // Knx library version

#include "HardwareSerial.h"
#include "Arduino.h"

#include "KnxTelegram.h"

// Services from TPUART
#define TPUART_RESET_INDICATION_BYTE B00000011 // 0x03

#define TPUART_DATA_CONFIRM_FAILED_BYTE B00001011 // 0x0B
#define TPUART_DATA_CONFIRM_SUCCESS_BYTE B10001011 // 0x8B

// Services to TPUART
#define TPUART_RESET_REQUEST B00000001 // 0x01
#define TPUART_STATE_REQUEST B00000010 // 0x02

#define TPUART_ACK_SERVICE_NOT_ADDRESSED B00010000 // 0x10
#define TPUART_ACK_SERVICE_ADDRESSED B00010001 // 0x11

#define TPUART_SET_ADDRESS_REQUEST B00101000 // 0x28

#define TPUART_DATA_END_REQUEST B01000000 // 0x40

#define TPUART_DATA_START_CONTINUE_REQUEST B10000000 // 0x80

// Debugging
// uncomment the following line to enable debugging
//#define TPUART_DEBUG

#define TPUART_DEBUG_PORT Serial

#define TPUART_SERIAL_CLASS Stream

// Delay in ms between sending of packets to the KNX
#define SERIAL_WRITE_DELAY_MS 100

// Timeout for reading a byte from TPUART
#define SERIAL_READ_TIMEOUT_MS 10

// Maximum number of group addresses that can be listened on
#define MAX_LISTEN_GROUP_ADDRESSES 48

// Macros for converting PA and GA to 2-byte
#define PA_INTEGER(area, line, member) (byte*)(const byte[]){(area << 4) | line, member}

#define GA_INTEGER(area, line, member) (byte*)(const byte[]){(area << 3) | line, member}

enum KnxTpUartSerialEventType {
  TPUART_RESET_INDICATION,
  KNX_TELEGRAM,
  IRRELEVANT_KNX_TELEGRAM,
  UNKNOWN
};

class KnxTpUart {

  public:

    KnxTpUart(TPUART_SERIAL_CLASS*, byte*);
    void uartReset();
    void uartStateRequest();
    void addListenGroupAddress(byte* groupAddress);
    KnxTpUartSerialEventType serialEvent();
    KnxTelegram* getReceivedTelegram();

    bool groupWriteBool(byte* groupAddress, bool);
    bool groupWrite4BitInt(byte* groupAddress, int);
    bool groupWrite4BitDim(byte* groupAddress, bool, byte);
    bool groupWrite1ByteInt(byte* groupAddress, int);
    bool groupWrite2ByteInt(byte* groupAddress, int);
    bool groupWrite2ByteFloat(byte* groupAddress, float);
    bool groupWrite3ByteTime(byte* groupAddress, int, int, int, int);
    bool groupWrite3ByteDate(byte* groupAddress, int, int, int);
    bool groupWrite4ByteFloat(byte* groupAddress, float);
    bool groupWrite14ByteText(byte* groupAddress, String);

    bool groupAnswerBool(byte* groupAddress, bool);
    /*
      bool groupAnswer4BitInt(byte* groupAddress, int);
      bool groupAnswer4BitDim(byte* groupAddress, bool, byte);
    */
    bool groupAnswer1ByteInt(byte* groupAddress, int);
    bool groupAnswer2ByteInt(byte* groupAddress, int);
    bool groupAnswer2ByteFloat(byte* groupAddress, float);
    bool groupAnswer3ByteTime(byte* groupAddress, int, int, int, int);
    bool groupAnswer3ByteDate(byte* groupAddress, int, int, int);
    bool groupAnswer4ByteFloat(byte* groupAddress, float);
    bool groupAnswer14ByteText(byte* groupAddress, String);

    bool groupRead(byte* groupAddress);

    void setListenToBroadcasts(bool);
    void setIndividualAddress(byte*);
    bool individualAnswerAddress();
    bool individualAnswerMaskVersion(int, int, int);
    bool individualAnswerAuth(int, int, int, int, int);

  private:

    Stream* _serialport;
    KnxTelegram* _tg;
    KnxTelegram* _tg_ptp;

    bool _listen_to_broadcasts;
    bool _confirmation_timeout;
    byte _individual_address[2];
    byte _listen_group_addresses[MAX_LISTEN_GROUP_ADDRESSES][2];
    byte _listen_group_address_count;

    void sendAck();
    void sendNotAddressed();
    bool isListeningToGroupAddress(byte* groupAddress);
    bool isKNXControlByte(int);
    void checkErrors();
    void printByte(int);
    bool readKNXTelegram();
    void createKNXMessageFrame(int, KnxCommandType, byte* targetGroupAddress, int);
    void createKNXMessageFrameIndividual(int, KnxCommandType, byte* targetIndividualAddress, int);
    bool sendMessage();
    bool sendNCDPosConfirm(int, byte* targetIndividualAddress);
    int serialRead();
};

#endif