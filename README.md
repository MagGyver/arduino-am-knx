Arduino EIB/KNX Interface via TP-UART
=====================================


This is a very first version of an interface between Arduino and EIB/KNX using the TP-UART interface and Arduino library.

Last modified: 13.08.2017


V 1.00 - Optimizing memory consumption

V 1.01 - DPT 3 added

V 1.02 - Fixed error on initialization of serial port

V 1.03 - Added link to library

V 1.04 - Structure of versions changed

V 1.05 - Additions with define added

Hardware
--------

We leverage Siemens BCU 5WG1 117-2AB12 - for about ~30€


We leverage ARDUINO MEGA (SERIAL PORT 1)

-> No restrictions on the programming


We leverage ARDUINO UNO (SERIAL PORT)

-> During programming, the BCU may have no connection to the ARDUINO UNO.

-> After programming for communication with the BCU. Connect the jumpers ICSP1 PIN 5 and ICSP1 PIN 6 together, tested only with ARDUINO UNO revision 3.

-> For programming the jumper ICSP1 PIN 5 and ICSP1 PIN 6 must be not connected together and the voltage must be taken away for a short time. Then, you can transfer the new "sketch". 


Software
--------

The Arduino library is found in the directory `KnxTpUart` and can be directly placed in Arduino's library folder. 


Issues, Comments and Suggestions
--------------------------------

If you have any, don't hesitate to contact me or use the issue tracker. You are also invited to improve the code and send me a pull request to reintegrate the changes here.


Supported commands / telegram types
-----------------------------------
All commando (write) to bus :
-----------------------------

Bool (DPT 1 - 0 or 1)

knx.groupWriteBool(GA_INTEGER(1, 2, 3), bool);



4 Bit Int (DPT 3)

knx.groupWrite4BitInt(GA_INTEGER(1, 2, 3), int);



4 Bit Dim (DPT 3)

knx.groupWrite4BitDim(GA_INTEGER(1, 2, 3), direction, steps);



1 Byte Int (DPT 5 - 0...255)

knx.groupWrite1ByteInt(GA_INTEGER(1, 2, 3), int);



2 Byte Int (DPT 7 - 0…65 535])

knx.groupWrite2ByteInt(GA_INTEGER(1, 2, 3), int);



2 Byte Float (DPT 9 - -671 088,64 to 670 760,96 )

knx.groupWrite2ByteFloat(GA_INTEGER(1, 2, 3), float);



3 Byte Time (DPT 10)

groupWrite3ByteTime(GA_INTEGER(1, 2, 3), Weekday, Hour, Minute, Second);



3 Byte Date (DPT 11)

groupWrite3ByteDate(GA_INTEGER(1, 2, 3), Day, Month, Year);



4 byte Float (DPT 14 - -2147483648 to 2147483647) 

knx.groupWrite4ByteFloat(GA_INTEGER(1, 2, 3), float);



14 Byte Text (DPT 16)

knx.groupWrite14ByteText(GA_INTEGER(1, 2, 3), String);



All commando (answer) to bus :
------------------------------

Bool (DPT 1 - 0 or 1)

knx.groupAnswerBool(GA_INTEGER(1, 2, 3), bool);



4 Bit Int (DPT 3)

commented out -> knx.groupAnswer4BitInt(GA_INTEGER(1, 2, 3), int);



4 Bit Int (DPT 3)

commented out -> knx.groupWrite4BitDim(GA_INTEGER(1, 2, 3), bool, byte);



1 Byte Int (DPT 5 - 0...255)

knx.groupAnswer1ByteInt(GA_INTEGER(1, 2, 3), int);



2 Byte Int (DPT 7 - 0…65 535])

knx.groupAnswer2ByteInt(GA_INTEGER(1, 2, 3), int);



2 Byte Float (DPT 9 - -671 088,64 to 670 760,96 )

knx.groupAnswer2ByteFloat(GA_INTEGER(1, 2, 3), float);



3 Byte Time (DPT 10)

knx.groupAnswer3ByteTime(GA_INTEGER(1, 2, 3), int, int, int, int);



3 Byte Date (DPT 11)

knx.groupAnswer3ByteDate(GA_INTEGER(1, 2, 3), int, int, int);



4 byte Float (DPT 14 - -2147483648 to 2147483647)

knx.groupAnswer4ByteFloat(GA_INTEGER(1, 2, 3), float);



14 Byte Text (DPT 16)

knx.groupAnswer14ByteText(GA_INTEGER(1, 2, 3), String);


Once for all DPT -> commando (read) to bus :
--------------------------------------------

knx.groupRead(GA_INTEGER(1, 2, 3));



Evaluation telegrams from bus :
-------------------------------

Bool (DPT 1 - 0 or 1)

value = telegram->getBool();



4 Bit Int (DPT 3)

value = telegram->get4BitIntValue();



Bool (DPT 3)

value = telegram->get4BitDirectionValue();



1 byte (DPT 3)

value = telegram->get4BitStepsValue();



1 Byte Int (DPT 5 - 0...255)

value = telegram->get1ByteIntValue();



2 Byte Int (DPT 7 - 0…65 535])

value = telegram->get2ByteIntValue();



2 Byte Float (DPT 9 - -671 088,64 to 670 760,96 )

value = telegram->get2ByteFloatValue();



3 Byte Time (DPT 10)

value = telegram->get3ByteWeekdayValue();



3 Byte Time (DPT 10)

value = telegram->get3ByteHourValue();



3 Byte Time (DPT 10)

value = telegram->get3ByteMinuteValue();



3 Byte Time (DPT 10)

value = telegram->get3ByteSecondValue();



3 Byte Time (DPT 11)

value = telegram->get3ByteDayValue();


3 Byte Time (DPT 11)

value = telegram->get3ByteMonthValue();



3 Byte Time (DPT 11)

value = telegram->get3ByteYearValue();



4 byte Float (DPT 14 - -2147483648 to 2147483647)

value = telegram->get4ByteFloatValue();



14 Byte Text (DPT 16)

value = telegram->get14ByteValue();